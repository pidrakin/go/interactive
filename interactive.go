package interactive

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/mattn/go-isatty"
	"gitlab.com/pidrakin/go/slices"
	"gitlab.com/pidrakin/go/templates"
	"golang.org/x/term"
)

var Debug bool = false

type Prompter struct {
	interactive bool
	reader      io.Reader
	writer      io.Writer
}

func (prompter *Prompter) Interactive() bool {
	return prompter.interactive
}

func (prompter *Prompter) SetInteractive(interactive bool) {
	prompter.interactive = interactive
}

func (prompter *Prompter) TmpInteractiveChange(f func() error) error {
	tmp := prompter.interactive
	if err := f(); err != nil {
		return err
	}
	prompter.interactive = tmp
	return nil
}

func (prompter *Prompter) TmpInteractive(f func() error) error {
	return prompter.TmpInteractiveChange(func() error {
		prompter.interactive = true
		return f()
	})
}

func Interactive(p *Prompter) error {
	p.interactive = true
	return nil
}

func NotInteractive(p *Prompter) error {
	p.interactive = false
	return nil
}

func SetInteractive(interactive bool) func(p *Prompter) error {
	return func(p *Prompter) error {
		p.interactive = interactive
		return nil
	}
}

func SetReader(reader io.Reader) func(p *Prompter) error {
	return func(p *Prompter) error {
		if reader != nil {
			p.reader = reader
		}
		return nil
	}
}

func SetWriter(writer io.Writer) func(p *Prompter) error {
	return func(p *Prompter) error {
		if writer != nil {
			p.writer = writer
		}
		return nil
	}
}

func NewPrompter(options ...func(*Prompter) error) (*Prompter, error) {
	p := &Prompter{}

	// Default values...
	p.interactive = true
	p.reader = os.Stdin
	p.writer = os.Stdout

	// Option parameters values:
	for _, op := range options {
		err := op(p)
		if err != nil {
			return nil, err
		}
	}
	return p, nil
}

func Prompt(reader io.Reader, delim byte, password bool) (response string, err error) {
	stdin := reader == os.Stdin
	seeker, ok := reader.(io.Seeker)

	var pos int64
	if !stdin && ok {
		pos, err = seeker.Seek(0, io.SeekCurrent)
		if err != nil {
			return
		}
	}

	if !stdin || !password {
		buffer := bufio.NewReader(reader)
		response, err = buffer.ReadString(delim)
		if err != nil {
			return
		}
	} else {
		bytePw, errPw := term.ReadPassword(int(os.Stdin.Fd()))
		if errPw != nil {
			return "", errPw
		}
		response = strings.TrimSpace(string(bytePw))
	}

	if !stdin && ok {
		_, err = seeker.Seek(pos+int64(len(response)), io.SeekStart)
	}
	return
}

func PromptLine(reader io.Reader, password bool) (string, error) {
	return Prompt(reader, '\n', password)
}

func testTerminal() bool {
	return Debug || isatty.IsTerminal(os.Stdout.Fd())
}

func (prompter *Prompter) YesNo(question string) (result bool, err error) {
	if !testTerminal() || !prompter.interactive {
		return false, nil
	}
	return yesNo(prompter.reader, prompter.writer, question)
}

func yesNo(reader io.Reader, writer io.Writer, question string) (result bool, err error) {
	_, err = fmt.Fprintf(writer, "%s [y/N]: ", question)
	if err != nil {
		return
	}
	response, err := PromptLine(reader, false)
	if err != nil {
		return
	}
	response = strings.TrimSuffix(response, "\n")

	if response == "" {
		return false, nil
	}
	result = strings.ToLower(strings.TrimSpace(response))[0] == 'y'
	return
}

func (prompter *Prompter) SingleChoice(options []string, question string) (index int, option string, err error) {
	if !testTerminal() || !prompter.interactive {
		return -1, option, nil
	}
	return singleChoice(prompter.reader, prompter.writer, options, question)
}

func singleChoice(reader io.Reader, writer io.Writer, options []string, question string) (index int, option string, err error) {
	header := []string{"Choice", "Name"}
	elements := append(options, header[1])
	lengths := slices.Map(elements, func(s string) int { return len(s) })
	maxLength := slices.Max(lengths)
	dashes := strings.Repeat("-", 2+len(header[0])+2+maxLength+2)
	data := templates.TemplateData{
		"header":   header,
		"dashes":   dashes,
		"names":    options,
		"question": question,
	}
	tmpl := `
{{- $header := .header }}
{{- range .header -}}
  {{ . | printf "  %s" }}
{{- end }}
{{ .dashes }}
{{- range $idx, $name := .names }}
  {{ $idx | printf (printf "%%-%vv" ((index $header 0) | len)) }}  {{ $name }}
{{- end }}
{{ .dashes }}
{{ .question }}: `
	parsed, _ := templates.Tprintf(tmpl, data)
	_, err = fmt.Fprintf(writer, parsed)
	response, err := PromptLine(reader, false)
	if err != nil {
		return
	}
	response = strings.TrimSuffix(response, "\n")
	index, err = strconv.Atoi(response)
	if err != nil {
		return
	}
	option = options[index]
	return
}
