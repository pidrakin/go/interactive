package interactive

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"strings"
	"testing"
)

func TestYesNo(t *testing.T) {
	answers := map[string]bool{
		"y":      true,
		"Y":      true,
		"yes":    true,
		"Yes":    true,
		"YES":    true,
		"yolo":   true,
		"":       false,
		"n":      false,
		"N":      false,
		"no":     false,
		"No":     false,
		"NO":     false,
		"nope":   false,
		"foobar": false,
	}
	for answer, expected := range answers {
		var buf bytes.Buffer
		reader := strings.NewReader(fmt.Sprintf("%s\n", answer))
		result, err := yesNo(reader, &buf, "Test Question")
		tests.AssertNoError(t, err)
		assert.Equal(t, expected, result)
	}
}

func TestSingleChoice(t *testing.T) {
	reader := strings.NewReader("1\n")
	var buf bytes.Buffer
	options := []string{"First", "Second"}
	idx, option, err := singleChoice(reader, &buf, options, "What will you choose?")
	tests.AssertNoError(t, err)

	output := buf.String()

	// assert
	assert.Equal(t, 1, idx)
	assert.Equal(t, options[1], option)

	expectedOutput := `  Choice  Name
------------------
  0       First
  1       Second
------------------
What will you choose?: `
	assert.Equal(t, expectedOutput, output)

	fmt.Printf("%s%d\n\n", output, idx)
}
